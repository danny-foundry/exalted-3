import ExaltedActor from "./entities/actor.js";
import ExaltedItem from "./entities/item.js";
import ExaltedActorSheet from "./sheets/actor-sheet.js";
import ExaltedItemSheet from "./sheets/item-sheet.js";
import ExaltedCombat from "./combat/combat.js";
import ExaltedCombatTracker from "./combat/combat-tracker.js";

import {handlebarsHelpers, loadHandlebarsPartials} from "./lib/handlebars.js";
import {bindings as wwBindings} from './custom/white-wolf.js';


// todo
//  need to support manual join battle rolls (for instance app + performance, perception instead of wits, re-rolls)

Hooks.once('init', () => {
    // system initialisation

    // setup global scope
    game.exalted = {

    };

    // debug
    // CONFIG.debug.hooks = true;

    // set config
    CONFIG.Actor.entityClass = ExaltedActor;
    CONFIG.Item.entityClass = ExaltedItem;
    CONFIG.Combat.entityClass = ExaltedCombat;
    CONFIG.ui.combat = ExaltedCombatTracker;
    // CONFIG.time.roundTime = 1;
    // CONFIG.Combat.initiative.formula = "1d6 + @attributes.FP.value";

    // unregister core sheets
    Actors.unregisterSheet("core", ActorSheet);
    Items.unregisterSheet("core", ItemSheet);

    // register custom sheets
    Actors.registerSheet("exalted-3", ExaltedActorSheet, {
        types: ["solar"],
        makeDefault: true,
        label: "Solar",
    });

    Actors.registerSheet("exalted-3", ExaltedActorSheet, {
        types: ["lunar"],
        makeDefault: false,
        label: "Lunar",
    });

    Items.registerSheet("exalted-3", ExaltedItemSheet, {
        types: ["intimacy"],
        label: "Intimacy",
    });

    Items.registerSheet("exalted-3", ExaltedItemSheet, {
        types: ["solarCharm"],
        label: "Solar Charm",
    });

    Items.registerSheet("exalted-3", ExaltedItemSheet, {
        types: ["lunarCharm"],
        label: "Lunar Charm",
    });

    Items.registerSheet("exalted-3", ExaltedItemSheet, {
        types: ["merit"],
        label: "Merit",
    });
    Items.registerSheet("exalted-3", ExaltedItemSheet, {
        types: ["armour"],
        label: "Armour",
    });
    Items.registerSheet("exalted-3", ExaltedItemSheet, {
        types: ["equipment"],
        label: "Equipment",
    });
    Items.registerSheet("exalted-3", ExaltedItemSheet, {
        types: ["meleeWeapon"],
        label: "Melee Weapon",
    });
    Items.registerSheet("exalted-3", ExaltedItemSheet, {
        types: ["rangedWeapon"],
        label: "Ranged Weapon",
    });

    Items.registerSheet("exalted-3", ExaltedItemSheet, {
        types: ["shape"],
        label: "Shape",
    });

    Items.registerSheet("exalted-3", ExaltedItemSheet, {
        types: ["thrownWeapon"],
        label: "Thrown Weapon",
    });

    handlebarsHelpers();
    loadHandlebarsPartials();
});

Hooks.on("updateCombatant", (combat, combatant, diff, options, userId /*not sure about this*/) => {
    // console.log(combatant, diff);
    // debugger;
});

Hooks.on("updateCombat", (...args) => {
    // console.warn(args);
});

Hooks.on("preUpdateActor", (actor, updateData) => {
    setTimeout(() => {
        // todo, make this good
        wwBindings.announceAnimaLevelChanges(actor, updateData);
        //wwBindings.updateTokens(actor);
    }, 100);
});