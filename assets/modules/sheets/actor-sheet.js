import {bindings, getItemsByType, getItemsByTypes} from "../lib/sheet-helpers.js";
import {bindCharacterDiceRoller} from "../lib/dice.js";
import {fullPath} from "../lib/paths.js";
import {bindings as wwBindings} from "../custom/white-wolf.js";
import {tagCaptions} from "../custom/tags.js";
import {skills} from "../custom/skills.js";

function _tagList(tags) {
    let list = [];

    Object.entries(tags).forEach((entry) => {
        if (!entry[1]) {
            return;
        }

        list.push(tagCaptions[entry[0]]);
    });

    return list;
}

export default class ExaltedActorSheet extends ActorSheet {
    constructor(...args) {
        super(...args);
        // constructor extension
    }

    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            scrollY: [
                ".inventory .inventory-list", // todo - check what this actually does
            ],
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-1" }], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.actor.limited; // permissions
        let type = this.actor.data.type;

        if (isLimited) {
            return fullPath('templates/actors/limited-sheet.html');
        }
        return fullPath(`templates/actors/${type}-sheet.html`);
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        let categorisedCharms = {};
        let charmTrees = [];

        if (this.actor.data.type === 'solar') {
            getItemsByType(this.actor.data, 'solarCharm', true).forEach((charm) => {
                let skill = charm?.data?.skill || 'No Skill'

                if (!categorisedCharms[skill]) {
                    categorisedCharms[skill] = [];
                }
                categorisedCharms[skill].push(charm);
            });
            charmTrees = Object.values(this.actor.data.data.skills).map((skill) => skill.name);
        } else if (this.actor.data.type === 'lunar') {
            getItemsByType(this.actor.data, 'lunarCharm', true).forEach((charm) => {
                let attribute = charm?.data?.attribute || 'Universal'

                if (!categorisedCharms[attribute]) {
                    categorisedCharms[attribute] = [];
                }
                categorisedCharms[attribute].push(charm);
            });
            charmTrees = [
                "universal",
                "strength",
                "dexterity",
                "stamina",
                "charisma",
                "manipulation",
                "appearance",
                "perception",
                "intelligence",
                "wits"
            ];
        }

        return {
            abilities: skills,
            actor: this.actor.data,
            characteristics: [
                'strength',
                'dexterity',
                'stamina',
                'charisma',
                'manipulation',
                'appearance',
                'perception',
                'intelligence',
                'wits',
                ...Object.keys(skills),
            ],
            charms: categorisedCharms,
            charmTrees: charmTrees,
            exaltType: this.actor.getTypeName(),
            intimacies: getItemsByType(this.actor.data, 'intimacy').map((intimacy) => {
                switch(intimacy.strength) {
                    case 2:
                        intimacy.strength_name = 'Minor';
                        break;
                    case 3:
                        intimacy.strength_name = 'Major';
                        break;
                    case 4:
                        intimacy.strength_name = 'Defining';
                        break;
                    default:
                        intimacy.strength_name = '';
                        break;
                }
                return intimacy;
            }),
            merits: getItemsByType(this.actor.data, 'merit'),
            shapes: getItemsByType(this.actor.data, 'shape'),
            shape: this.actor.getOwnedItem(this.actor.data.data.shape_id),
            tagCaptions: tagCaptions,
            armour: getItemsByType(this.actor.data, 'armour').map((item) => {
                item.tags = _tagList(item.data.tags);
                return item;
            }),
            equipment: getItemsByType(this.actor.data, 'equipment'),
            weapons: getItemsByTypes(this.actor.data, ['meleeWeapon', 'thrownWeapon', 'rangedWeapon']).map((weapon) => {
                let types = { melee: false, thrown: false, ranged: false };
                if (weapon.type === 'meleeWeapon') types.melee = true;
                else if (weapon.type === 'thrownWeapon') types.thrown = true;
                else if (weapon.type === 'rangedWeapon') types.ranged = true;

                weapon.types = types;
                weapon.tags = _tagList(weapon.data.tags);

                return weapon;
            }),
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        super.activateListeners($doc);
        if (!this.isEditable) {
            return;
        }

        bindings.selectAllOnFocus($doc);
        bindings.editButtons($doc, this.actor);
        bindings.deleteButtons($doc, this.actor);
        bindings.inlineItemEdit($doc, this.actor);
        bindings.debug($doc, this.object);
        bindings.itemCreateButton($doc, this.actor);
        wwBindings.dots($doc, this.actor);
        wwBindings.essence($doc, this.actor);
        wwBindings.charmFilter($doc);
        wwBindings.damage($doc, this.actor);

        // only the owner can roll
        if (this.actor.owner) {
            bindCharacterDiceRoller($doc, this.actor);
        } else {
            $doc.find('.rollable').removeClass('rollable');
        }
    }

    /* Validate values, enforce max/min etc */
    /** @override */
    _updateObject(evt, form) {
        return this.object.update(form);
    }
}