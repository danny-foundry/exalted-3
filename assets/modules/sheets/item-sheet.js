import {bindings} from "../lib/sheet-helpers.js";
import {bindings as wwBindings} from "../custom/white-wolf.js";
import {fullPath} from "../lib/paths.js";
import {skills} from "../custom/skills.js";
import {chunkArrayIntoColumns} from "../lib/object.js";
import {tagCaptions} from "../custom/tags.js";

// todo - this is getting quite heavy with the number of items we have, maybe split them out into multiple classes?
// todo - break out the stats for other shapes: join battle, attack(s) - other pools just get listed
export default class ExaltedItemSheet extends ActorSheet {
    constructor(...args) {
        super(...args);

        // constructor extension
    }

    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            scrollY: [
                ".inventory .inventory-list", // todo - check what this actually does
            ],
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.object.limited; // permissions
        let type = this.object.type;

        return fullPath(`templates/items/${type}-sheet.html`);
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        let data = {
            item: this.object.data,
            hasActor: !!this.object.options.actor,
            tags: {
                columns: [],
                captions: tagCaptions,
            },
        };

        if (this.object.type === 'solarCharm') {
            data.skills = Object.keys(skills);
        } else if (this.object.type === 'lunarCharm') {
            data.attributes = [
                "universal",
                "strength",
                "dexterity",
                "stamina",
                "charisma",
                "manipulation",
                "appearance",
                "perception",
                "intelligence",
                "wits"
            ];
        } else if (this.object.type === 'shape') {
            data.poolSources = [
                'strength',
                'dexterity',
                'stamina',
                'charisma',
                'manipulation',
                'appearance',
                'perception',
                'intelligence',
                'wits',
                ...Object.keys(skills),
            ];
        }

        if (this.object.data.data.tags) {
            data.tags.columns = chunkArrayIntoColumns(Object.keys(this.object.data.data.tags), 3);
        }

        return data;
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        const sheet = this;

        if (!this.isEditable) {
            return;
        }

        bindings.selectAllOnFocus($doc);
        bindings.debug($doc, this.object);
        wwBindings.dots($doc);
        wwBindings.animalPools($doc, this.object);

        super.activateListeners($doc);
    }

    /* Validate values, enforce max/min etc */
    /** @override */
    _updateObject(evt, form) {
        return this.object.update(form);
    }
}