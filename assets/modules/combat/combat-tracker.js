// based on https://github.com/BoltsJ/lancer-initiative/blob/default/src/module/lancer-combat-tracker.ts
import {roll} from "../lib/dice.js";

export default class ExaltedCombatTracker extends CombatTracker {
    /**
     * Make all the changes to the combat tracker before setting up event
     * handlers.
     * @override
     */
    async _renderInner(data/*: LancerCombatTracker.Data*/)/*: Promise<JQuery<HTMLElement>>*/ {
        const hasActed = $('<i style="color: green; display: inline-block; margin-left: 5px;" class="fas fa-check"></i>');
        const hasNotActed = $('<i style="color: #777; display: inline-block; margin-left: 5px;" class="fas fa-check"></i>');
        const crash = $('<i style="display: inline-block; margin-left: 5px;" class="far fa-tired"></i>');

        const html = await super._renderInner(data);

        html.find(".combatant").each(function () {
            const combatantId = $(this).data("combatantId");
            const combatant = data.combat.getCombatant(combatantId);
            const initiative = $('<span>' + combatant.initiative + '</span>');
            const rollButton = $('<button class="item-button" data-action="roll-initiative"><i class="fas fa-dice"></i></button>');
            const changeButton = $('<button class="item-button" data-action="change-initiative"><i class="fas fa-plus"></i></button>');
            const acted = combatant.has_acted ? hasActed : hasNotActed;


            // render icons
            if (combatant.initiative) {
                $(this)
                    .find(".token-initiative")
                    .empty()
                    .append(initiative)
                    .append(changeButton);
                $(this).find('.token-name h4').append(acted.clone());

                if (combatant.initiative < 0) {
                    $(this).find('.token-name h4').append(crash.clone()).append(`<span style="display:inline-block; margin-left: 5px;">${combatant.turns_in_crash}</span>`);
                }
            } else {
                // button to roll initiative
                $(this)
                    .find(".token-initiative")
                    .empty()
                    .append(rollButton);
            }
        });
        return html;
    }

    /** @override */
    activateListeners($doc/*: JQuery<HTMLElement>*/)/*: void*/ {
        super.activateListeners($doc);

        if (!this.combat || !this.combat.data) {
            return;
        }

        const self = this;
        const combatants = this.combat.data.combatants;
        $doc.find('[data-action="roll-initiative"]').on("click", function() {
            const $container = $(this).closest('[data-combatant-id]');
            const id = $container.data('combatant-id');

            const combatant = combatants.find((cmb) => {
                return cmb._id === id;
            });

            self.rollInitiative(combatant);
        });

        $doc.find('[data-action="change-initiative"]').on('click', function() {
            const $container = $(this).closest('[data-combatant-id]');
            const id = $container.data('combatant-id');
            const combatant = combatants.find((cmb) => {
                return cmb._id === id;
            });
            let modifier = parseInt(prompt('Enter change in initiative'));
            self.combat.setInitiative(combatant._id, combatant.initiative + modifier);
        });
    }

    /** @override */
     _getEntryContextOptions()/*: ContextMenu.Item[]*/ {
        return super._getEntryContextOptions();
    }

    rollInitiative(combatant) {
        const self = this;

        let pool = 0;
        let joinBattle = combatant.actor.data.data.joinBattle;
        console.log(joinBattle);
        if (!joinBattle) {
            joinBattle = {
                characteristic1: 'wits',
                characteristic2: 'Awareness',
                modifier: 0
            };
        }

        if (joinBattle.characteristic1) {
            pool += combatant.actor.getCharacteristic(joinBattle.characteristic1);
        }
        if (joinBattle.characteristic2) {
            pool += combatant.actor.getCharacteristic(joinBattle.characteristic2);
        }

        if (joinBattle.modifier) {
            pool += parseInt(joinBattle.modifier);
        }

        console.log(joinBattle, pool);

        roll(pool).then((init) => {
            let initiative = init.successes + 3;

            self.combat.setInitiative(combatant._id, initiative);
            CONFIG.ChatMessage.entityClass.create({
                user: game.user._id,
                speaker: ChatMessage.getSpeaker({ actor: combatant.actor }),
                type: CONST.CHAT_MESSAGE_TYPES.IC,
                flavor: 'Rolling initiative',
                content: init.dice.join(", ") + ' - ' + init.successes + ' successes (+3)',
                sound: CONFIG.sounds.dice,
            });
        });
    }
}