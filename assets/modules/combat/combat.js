// based on https://github.com/BoltsJ/lancer-initiative/blob/default/src/module/lancer-combat.ts
import {sortCombatants, sortNumbers} from "../custom/white-wolf.js";

export default class ExaltedCombat extends Combat {
    hasActed = [];
    currentToken = '';

    // todo - would it be easier to store initiative changes for players that have already been (and for players that would advance to before the current tick)
    //  and apply them at the end of the round?
    /** @override */
    _prepareCombatant(combatant, /*: Combat.Combatant,*/scene, /*: Scene,*/players, /*: User[],*/settings, /*: unknown = {}*/)/*: Combat.Combatant*/ {
        combatant = super._prepareCombatant(combatant, scene, players, settings);

        //const index = this.hasActed ? this.hasActed.indexOf(combatant.token._id) : -1;
        combatant.has_acted = combatant.has_acted || false;
        combatant.acted_on_tick = combatant.acted_on_tick || -1;
        combatant.current = combatant.current || false;
        combatant.turns_in_crash = combatant.turns_in_crash || 0;

        return combatant;
    }

    /** @override */
    _sortCombatants(a/*: Combat.Combatant*/, b/*: Combat.Combatant*/)/*: number*/ {
        return sortCombatants(a, b);
    }

    /** @override */
    _onCreate(data/*: Combat.Data*/, options/*: Entity.CreateOptions*/, userId/*: string*/)/*: void*/ {
        super._onCreate(data, options, userId);
    }

    findNextCombatant() {
        const next = this.combatants.sort(this._sortCombatants).filter((c) => {
            if (c.has_acted || c.current) return false;
            return true;
        })[0];

        if (!next) {
            return this.combatants.sort((a, b) => sortNumbers(a.initiative, b.initiative, true))[0];
        }
        return next;
    }

    /** @override */
    async nextTurn() {
        super.nextTurn();

        const nextCombatant = this.findNextCombatant();
        const lastTokenId = this.current.tokenId;

        if (lastTokenId) {
            let combatant = this.getCombatantByToken(lastTokenId);
            let turnsInCrash = combatant.turns_in_crash;

            console.log(combatant.actor.data.name);
            if (combatant.actor.data.name === 'Richard') {
                console.log(combatant.initiative, combatant.start_of_turn_initiative);
            }

            if (combatant.initiative < 0 && combatant.start_of_turn_initiative < 0) {
                turnsInCrash ++;
            } else if (combatant.initiative >= 0){
                turnsInCrash = 0;
            }
            game.combat.updateCombatant({
                _id: combatant._id,
                turns_in_crash: turnsInCrash,
            });
        }

        if (lastTokenId && this.data.turn < this.data.combatants.length - 1) {
            this.hasActed.push(lastTokenId);

            let combatant = this.getCombatantByToken(lastTokenId);
            game.combat.updateCombatant({
                _id: combatant._id,
                has_acted: true,
                current: false,
                acted_on_tick: this.hasActed.length,
            });
        }

        let turnsInCrash = nextCombatant.turns_in_crash;
        let initiative = nextCombatant.initiative;

        if (initiative < 0 && turnsInCrash >= 3) {
            initiative = 3;
            turnsInCrash = 0;
        }

        game.combat.updateCombatant({
            _id: nextCombatant._id,
            start_of_turn_initiative: nextCombatant.initiative,
            has_acted: false,
            current: true,
            acted_on_tick: -1,
            initiative: initiative,
            turns_in_crash: turnsInCrash,
        });
    }

    /** @override */
    async previousTurn() {
        return new Promise((resolve,reject) => {
            reject();
        });
    }

    /** @override */
    async startCombat()/*: Promise<this>*/ {
        this.findNextCombatant().current = true;
        return super.startCombat();
    }

    /** @override */
    async nextRound()/*: Promise<void>*/ {
        this.resetHasActed();

        return super.nextRound();
    }

    /** @override */
    async previousRound()/*: Promise<this>*/ {
        return new Promise((resolve,reject) => {
            reject();
        });
    }

    /** @override */
    async resetAll()/*: Promise<this>*/ {
        return super.resetAll();
    }

    resetHasActed() {
        this.hasActed = [];
        //this.currentToken = '';

        this.combatants.forEach((combatant) => {
            game.combat.updateCombatant({
                _id: combatant._id,
                has_acted: false,
                current: false,
                acted_on_tick: -1
            });
        });
    }
}