import {skills} from "../custom/skills.js";
import {fullPath} from "../lib/paths.js";

export default class ExaltedActor extends Actor {
    // ensure the object matches the schema
    prepareBaseData() {
        this.data.data.skills = this.data.data.skills || {};
        this.data.data.additional_skills = this.data.data.additional_skills || [];
        this.data.data.specialties = this.data.data.specialties || [];
        this.data.data.anima_level = this.data.data.anima_level || 0;

        this.data.data.experience = this.data.data.experience || { earnt: 0, spent: 0};
        this.data.data.solar_experience = this.data.data.solar_experience || { earnt : 0, spent : 0};

        this.data.data.experience.available = this.data.data.experience.earnt - this.data.data.experience.spent;
        this.data.data.solar_experience.available = this.data.data.solar_experience.earnt - this.data.data.solar_experience.spent;

        for (var skill in skills) {
            let model = $.extend({
                name: skill,
                attribute: skills[skill],
                level: 0,
                favoured: false
            }, this.data.data.skills[skill]);
            this.data.data.skills[skill] = model;
        }

        while(this.data.data.additional_skills.length < 4) {
            this.data.data.additional_skills.push({
                name: '',
                level: 0,
                favoured: false,
            });
        }

        while(this.data.data.specialties.length < 4) {
            this.data.data.specialties.push('');
        }
    }

    // set any computed attributes
    prepareDerivedData() {
        this.data.total_damage = 0;
        this.data.total_health = 1; // incapped

        Object.values(this.data.data.health_levels || {}).forEach((count) => {
            this.data.total_health += parseInt(count);
        });

        Object.values(this.data.data.damage || {}).forEach((count) => {
            this.data.total_damage += parseInt(count);
        });

        // todo - wound penalties?
    }

    getCharacteristic(characteristic) {
        if (skills[characteristic]) {
            return parseInt(this.data.data.skills[characteristic].level);
        }
        return parseInt(this.data.data[characteristic].value);
    }

    getCharmDiceCap(attribute, ability, advanced) {
        let attrValue = attribute ? this.getCharacteristic(attribute) : 0;
        let abilityValue = ability ? this.getCharacteristic(ability) : 0;
        let secondaryAttrValue = 0;

        if (advanced.secondary_attribute) {
            secondaryAttrValue = this.getCharacteristic(advanced.secondary_attribute);
        }

        if (this.data.type === 'solar') {
            return attrValue + abilityValue;
        } else if (this.data.type === 'lunar') {
            return attrValue + secondaryAttrValue;
        }
    }

    getTypeName() {
        if (this.data.type === 'solar') {
            return 'Solar';
        }
        if (this.data.type === 'lunar') {
            return 'Lunar';
        }
        return 'Exalt';
    }

    updateTokens() {
        const shape = this.data.data.shape_id;
        let animaLevel = this.data.data.anima_level;
        let effectIcons = [
            '',
            fullPath('assets/img/glowing.png'),
            fullPath('assets/img/burning.png'),
            fullPath('assets/img/bonfire.png'),
        ];

        let tints = [
            '#ffffff',
            '#bcb379',
            '#cdc460',
            '#cdbd00',
        ];
        const image = shape ? this.getOwnedItem(shape).img : this.data.token.img;

        this.getActiveTokens(true).forEach((token) => {
            token.scene.updateEmbeddedEntity(Token.embeddedName, {
                _id: token.data._id,
                img: image,
                tint: tints[animaLevel] || '#ffffff'
            });
        })
    }

    /** @override */
    _onUpdate(data, options, userId, context) {
        super._onUpdate(data, options, userId, context);
        this.updateTokens();
    }
}