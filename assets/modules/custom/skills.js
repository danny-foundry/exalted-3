export const skills = {
    "Archery": "dexterity",
    "Athletics": "dexterity",
    "Awareness": "perception",
    "Brawl": "dexterity",
    "Bureaucracy": "intelligence",
    "Craft": "intelligence",
    "Dodge": "dexterity",
    "Integrity": "wits",
    "Investigation": "perception",
    "Larceny": "dexterity",
    "Linguistics": "intelligence",
    "Lore": "intelligence",
    "Martial Arts": "dexterity",
    "Medicine": "intelligence",
    "Melee": "dexterity",
    "Occult": "intelligence",
    "Performance": "charisma",
    "Presence": "charisma",
    "Resistance": "stamina",
    "Ride": "dexterity",
    "Sail": "strength",
    "Socialize": "charisma",
    "Stealth": "dexterity",
    "Survival": "wits",
    "Thrown": "dexterity",
    "War": "intelligence",
};