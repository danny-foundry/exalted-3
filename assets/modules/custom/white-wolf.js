import {deepGet, deepSet} from "../lib/object.js";
import {fullPath} from "../lib/paths.js";
import {bindings as sheetBindings} from '../lib/sheet-helpers.js';

export function sortNumbers(a, b, descending = false) {
    let result = 0;

    if (a > b) {
        result = 1;
    } else if (b > a) {
        result = -1;
    } else {
        result = 0;
    }

    if (descending) {
        result = result * -1;
    }
    return result;
}

export function sortCombatants(combatantA, combatantB) {
    if (combatantA.acted_on_tick > -1 && combatantB.acted_on_tick > -1) {
        return sortNumbers(combatantA.acted_on_tick, combatantB.acted_on_tick);
    } else if (combatantA.acted_on_tick > -1) {
        return -1;
    } else if (combatantB.acted_on_tick > -1) {
        return 1;
    }

    if (combatantA.current && combatantB.current) {
        return 0;
    } else if (combatantA.current) {
        return -1;
    } else if (combatantB.current) {
        return 1;
    }

    return sortNumbers(combatantA.initiative, combatantB.initiative, true);
}

function handleDots($container, $dot, current) {
    const min = parseInt($container.data('min')) || 0;
    const max = parseInt($container.data('max')) || 5;
    let value = parseInt($dot.data('value'));

    if (value === parseInt(current)) {
        value --;
    }

    if (value < min) value = parseInt(min);
    if (value > max) value = parseInt(max);

    $container.find('.dot').each(function() {
        let dotVal = parseInt($(this).data('value'));

        if (dotVal <= value) {
            $(this).addClass('filled');
        } else {
            $(this).removeClass('filled');
        }
    });

    return parseInt(value);
}

function handleBoundDots(actor, $container, $dot) {
    const key = $container.data('name');
    const current = deepGet(actor.data, key.split('.'));
    let updates = {};

    deepSet(updates, key.split('.'), handleDots($container, $dot, current));
    actor.update(updates);
}

async function handleOwnedItemDots(actor, $container, $dot) {
    const itemId = $container.data('item-id');
    const key = $container.data('name');
    const current = deepGet(actor.getOwnedItem(itemId), key.split('.'));
    let updates = {};

    deepSet(updates, key.split('.'), handleDots($container, $dot, current));
    actor.getOwnedItem(itemId).update(updates);
}

export function spendEssence(actor, amount, source, muted = false) {
    let personal = actor.data.data.personal_essence;
    let peripheral = actor.data.data.peripheral_essence;
    let animaLevel = actor.data.data.anima_level;

    let personalAvailable = personal.value;
    let peripheralAvailable = peripheral.value;
    let personalSpend = 0;
    let peripheralSpend = 0;

    if (source === 'personal') {
        peripheralSpend = Math.max(0, amount - personalAvailable);
        personalSpend = Math.min(amount, personalAvailable);
    } else {
        personalSpend = Math.max(0, amount - peripheralAvailable);
        peripheralSpend = Math.min(amount, peripheralAvailable);
    }

    if (personalSpend > personalAvailable || peripheralSpend > peripheralAvailable) {
        return false;
    }

    if (!muted) {
        let animaIncrease = Math.floor(peripheralSpend / 5);
        animaLevel = Math.min(3, animaLevel + animaIncrease);
    }

    let updates = {
        data: {
            anima_level: animaLevel,
            personal_essence: {
                value: personalAvailable - personalSpend
            },
            peripheral_essence: {
                value: peripheralAvailable - peripheralSpend
            }
        }
    };
    actor.update(updates);
    bindings.updateTokens(actor);

    return true;
}

export const bindings = {
    announceAnimaLevelChanges(actor, changes) {
        if (changes.data === undefined || changes.data.anima_level === undefined) {
            return;
        }

        let name = actor.data.data.possessive_name;
        if (!name) {
            name = actor.name;
            if (name.endsWith('s')) {
                name += "'";
            } else {
                name += "'s";
            }
        }

        let text = '';
        // todo - make the wording of the emotes better, need to pluralise the name? configurable plural form?
        // todo - could have configurable sounds effects (looks to be just a uri to a wav file)
        switch(parseInt(changes.data.anima_level)) {
            case 0: // dim
                text = `${name} caste mark fades away`;
                break;
            case 1: // glowing
                text = `${name} caste mark is glowing brightly`;
                break;
            case 2: // burning
                text = `${name} caste mark burns brightly, illuminating the area`;
                break;
            case 3: // bonfire / iconic
                text = actor.data.data.anima_description;
                if (!text) {
                    text = `${name} anima banner blazes forth`;
                }
                break;
            default:
                return;
        }

        CONFIG.ChatMessage.entityClass.create({
            user: game.user._id,
            speaker: ChatMessage.getSpeaker({ actor: actor }),
            type: CONST.CHAT_MESSAGE_TYPES.EMOTE,
            content: text,
            sound: CONFIG.sounds.notification,
        });
    },
    updateTokens(actor) {
        // let animaLevel = actor.data.data.anima_level;
        // let tints = [
        //     '#ffffff',
        //     '#bcb379',
        //     '#cdc460',
        //     '#cdbd00',
        // ];
        //
        // getTokensFor(actor._id).forEach((token) => {
        //     const tint = tints[animaLevel] || '#ffffff';
        //
        //     token.scene.updateEmbeddedEntity(Token.embeddedName, {
        //         tint: tint,
        //         _id: token.data._id,
        //     });
        // });
    },
    dots($doc, actor = null) {
        $doc.on('click', '[data-control="dots"] .dot', function() {
            const $container = $(this).closest('[data-control="dots"]');
            if ($container.data('item-id')) {
                handleOwnedItemDots(actor, $container, $(this));
            } else {
                handleBoundDots(actor, $container, $(this));
            }
        });
    },
    essence($doc, actor = null) {
        $doc.on('click', '[data-action="spend-essence"]', async function() {
            let details = await sheetBindings.popupForm('Spend Essence', fullPath('templates/dialogs/spend-essence.html'));
            let amount = parseInt(details.amount);

            if (!spendEssence(actor, amount, details.source, details.mute)) {
                alert(`you do not have ${amount} essence to spend`);
            }
        });

        $doc.on('click', '[data-action="gain-essence"]', function() {
            let amount = parseInt(prompt("Amount of essence to gain"));
            let personal = actor.data.data.personal_essence;
            let peripheral = actor.data.data.peripheral_essence;

            let peripheralRequired = peripheral.max - peripheral.value;
            if (peripheralRequired >= amount) {
                peripheral.value = amount;
                amount = 0;
            } else {
                peripheral.value += peripheralRequired;
                amount = amount - peripheralRequired;
            }

            personal.value = Math.min(personal.max, personal.value + amount);
            let data = {
                data: {
                    personal_essence: {
                        value: personal.value
                    },
                    peripheral_essence: {
                        value: peripheral.value
                    }
                }
            };

            actor.update(data);
        });
    },
    damage($doc, actor) {
        $doc.find('[data-action="take-damage"]').on('click', async function() {
            let details = await sheetBindings.popupForm('Take damage', fullPath('templates/dialogs/health-and-damage.html'), {title: 'Take damage'});
            let bashing = parseInt(details.bashing);
            let lethal = parseInt(details.lethal);
            let aggravated = parseInt(details.aggravated);

            // todo - need to handle wrapping bashing damage into lethal

            // deal agg damage
            // deal lethal damage
            // deal bashing damage
            let damage = actor.data.data.damage || {};
            damage.aggravated = (damage.aggravated || 0) + aggravated;
            damage.lethal = (damage.lethal || 0) + lethal;
            damage.bashing = (damage.bashing || 0);

            let availableHealth = actor.data.total_health - damage.aggravated - damage.lethal - damage.bashing;
            if (availableHealth >= bashing) {
                damage.bashing += bashing;
            } else {
                damage.bashing += availableHealth;
                damage.lethal += bashing - availableHealth;
            }

            actor.update({
                data: {
                    damage: damage,
                }
            });
        });

        $doc.find('[data-action="heal-damage"]').on('click', async function() {
            let details = await sheetBindings.popupForm('Heal damage', fullPath('templates/dialogs/health-and-damage.html', { title: 'Heal damage'}));
            let bashing = parseInt(details.bashing);
            let lethal = parseInt(details.lethal);
            let aggravated = parseInt(details.aggravated);
            let damage = actor.data.data.damage || {};

            damage.aggravated = Math.max((damage.aggravated || 0) - aggravated, 0);
            damage.lethal = Math.max((damage.lethal || 0) - lethal, 0);
            damage.bashing = Math.max((damage.bashing || 0) - bashing, 0);

            actor.update({
                data: {
                    damage: damage,
                }
            });
        });
    },
    charmFilter($doc) {
        $doc.find('[data-role="charm-list"]').on('change', 'select,input', function() {
            const $container = $(this).closest('[data-role="charm-list"]');
            const skill = $container.find('[data-field="tree"]').val();
            const type = $container.find('[data-field="type"]').val();

            if (type) {
                $container.find('[data-type]').hide();
                $container.find(`[data-type="${type}"]`).show();
            } else {
                $container.find('[data-type]').show();
            }

            if (skill) {
                $container.find('[data-tree]').hide();
                $container.find(`[data-tree="${skill}"]`).show();
            } else {
                $container.find('[data-tree]').show();
            }

            $container.find('[data-tree]:visible').each(function() {
                if ($(this).find('[data-type]:visible').length === 0) {
                    $(this).hide();
                }
            });
        });
    },
    animalPools($doc, item) {
        $doc.find('[data-action="add-pool"]').on('click', function() {
            let pools = item.data.data.pools || {};
            let index = Object.keys(pools).length;

            pools[index] = {
                name: '',
                pool: 1
            };

            item.update({
                data: {
                    pools: pools,
                }
            });
        });
    }
};

export function renderDots(value, name, min, max, actor = null, itemId = null, style = '') {
    if (!min) {
        min = 0;
    }
    if (!max) {
        max = 5;
    }

    let styleClass = '';
    if (style === 'box') {
        styleClass = 'style-box';
    } else {
        styleClass = 'style-dot';
    }

    var $el = $('<div></div>');
    var $container = $(`<div class="dots ${styleClass}" data-control="dots" data-min="${min}" data-max="${max}"></div>`);
    $container.attr('data-name', name);

    if (itemId) {
        $container.attr('data-item-id', itemId);
    }

    for (let i = 1; i <= max; i ++) {
        let $dot = $(`<span class="dot" data-value="${i}"></span>`);
        if (i <= value) {
            $dot.addClass('filled');
        }
        $container.append($dot);
    }

    $el.append($container);
    return $el.html();
}