import {formToObject} from "./forms.js";
import {deepSet} from "./object.js";

export function getItemsByTypes(object, types, sorted = true) {
    let items = object.items.filter((item) => types.indexOf(item.type) !== -1);
    if (sorted) {
        return items.sort((a, b) => {
            return b.name > a.name ? -1 : 1;
        })
    }
    return items;
}

export function getItemsByType(object, type, sorted = true) {
    let items = object.items.filter((item) => item.type === type);
    if (sorted) {
        return items.sort((a, b) => {
            return b.name > a.name ? -1 : 1;
        })
    }
    return items;
}

export function getTokensFor(actorId) {
    let tokens = [];
    TokenLayer.instance.children.forEach((layerChild) => {
        layerChild.children.forEach((child) => {
            if (child.actor.data._id === actorId) {
                tokens.push(child);
            }
        });
    });

    return tokens;
}
window.getTokensFor = getTokensFor;

export const bindings = {
    // button binding to dump out the current object in console
    debug($doc, object) {
        $doc.find('[data-action="debug"]').on('click', () => {
            console.log(object.data);
        });
    },
    // select all when inputs focused
    selectAllOnFocus($doc) {
        $doc.find('input').focus(ev => ev.currentTarget.select());
    },
    // bind item edit buttons
    // expected attributes:
    //      data-action="edit-item"
    //      data-item-id="{{ item._id }}"
    editButtons($doc, actor) {
        $doc.find('[data-action="edit-item"]').click(function() {
            const itemId = $(this).data('item-id');
            actor.getOwnedItem(itemId).sheet.render(true);
        });
    },
    deleteButtons($doc, actor) {
        const self = this;

        $doc.find('[data-action="delete-item"]').click(async function() {
            const itemId = $(this).data('item-id');
            const name = actor.getOwnedItem(itemId).data.name;
            const confirmed = await self.confirm(`Delete '${name}'?`, 'systems/gurps-4/templates/dialogs/confirm.html', {
                name: name,
                action: 'delete',
            });

            if (confirmed) {
                actor.deleteOwnedItem(itemId);
            }
        });
    },
    popupForm(title, template, data = {}) {
        return new Promise((resolve, reject) => {
            $.get(template, async (content) => {
                let compiled = Handlebars.compile(content);
                await Dialog.prompt({
                    title: title,
                    content: compiled(data),
                    callback: async (dialog) => {
                        resolve(formToObject($('form', dialog)));
                    }
                });
            });
        });
    },
    confirm(title, template, data = {}) {
        return new Promise((resolve, reject) => {
            $.get(template, async (content) => {
                let compiled = Handlebars.compile(content);
                await Dialog.confirm({
                    title: title,
                    content: compiled(data),
                    yes: async() => {
                        resolve(true);
                    },
                    no: async() => {
                        resolve(false);
                    }
                });
            });
        });
    },
    inlineItemEdit($doc, actor) {
        $doc.on('change', '[data-bind="item"]', function() {
            let $this = $(this);
            let item = $this.data('item-id');
            let key = $this.data('key');
            let value = $this.val();
            let obj = {};

            deepSet(obj, key.split('.'), value);
            actor.getOwnedItem(item).update(obj);
        });
    },
    itemCreateButton($doc, actor) {
        $doc.on('click', '[data-action="add-item"]', function() {
            let type = $(this).data('item-type');

            let data = {
                name: 'Untitled',
                type: type,
                data: {

                }
            };

            actor.createOwnedItem(data);
        })
    },
};