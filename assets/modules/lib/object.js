
export function chunkArrayIntoColumns(array, columns) {
    let chunks = [];
    let length = array.length;
    const size = Math.ceil(length / columns);

    for (let i = 0; i < length; i += size) {
        chunks.push(array.slice(i, i + size));
    }

    return chunks;
}

export function chunkArray(array, size) {
    let chunks = [];
    let length = array.length;

    for (let i = 0; i < length; i += size) {
        chunks.push(array.slice(i, i + size));
    }

    return chunks;
}

export function deepSet(object, keys, value) {
    const key = keys.shift();
    if (keys.length === 0) {
        object[key] = value;
    } else {
        if (object[key] === undefined) {
            object[key] = {};
        }
        deepSet(object[key], keys, value);
    }
}

export function deepGet(object, keys) {
    const key = keys.shift();
    if (keys.length === 0) {
        return object[key];
    }

    if (object[key] === undefined) {
        return object[key];
    }

    return deepGet(object[key], keys);
}