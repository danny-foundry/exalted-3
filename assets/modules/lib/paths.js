const rootPath = 'systems/exalted-3/';

export function fullPath(rel) {
    return rootPath + rel;
}