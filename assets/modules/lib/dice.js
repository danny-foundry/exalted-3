import {fullPath} from "./paths.js";
import {bindings} from "./sheet-helpers.js";
import {skills} from "../custom/skills.js";

export const defaultRollOptions = {
    rerolls: [],
    doubles: [10],
};

export const advancedRollOptions = {
    charm_dice: 0,
    non_charm_dice: 0,
    rerolls: [],
    doubles: [],
    specialty: false,
    stunt_dice: 0,
};

function map(array, callback) {
    let mapped = [];

    for (var val in array) {
        if (!array.hasOwnProperty(val)) continue;
        mapped.push(callback(array[val]));
    }

    return mapped;
}

export async function roll(pool) {
    const roll = new Roll(`${pool}d10`, {});
    const result = roll.roll();
    const results = {
        successes: 0,
        dice: [],
    };

    result.dice[0].results.forEach((dice) => {
        if (dice.result >= 7) {
            results.successes ++;
        }
        results.dice.push(dice.result);
    });
    return results;
}

function RollResults() {
    this.initialPool = 0;
    this.totalRolled = 0;
    this.dice = [];
    this.successes = 0;
    this.initialSuccesses = 0;
    this.botches = 0;
}

async function rollDice(object, pool, options, results, initial = true) {
    const roll = new Roll(`${pool}d10`, object.getRollData());
    const result = roll.roll();
    let rerolls = 0;

    result.dice[0].results.forEach((dice) => {
        results.totalRolled ++;
        if (initial && dice.result > 6) {
            results.initialSuccesses ++;
        }

        if (options.doubles.indexOf(dice.result) > -1) {
            results.successes += 2;
        } else if (dice.result > 6) {
            results.successes += 1;
        } else if (dice.result === 1 && options.rerolls.indexOf(1) === -1) {
            results.botches += 1;
        }
        results.dice.push(dice.result);

        if (options.rerolls.indexOf(dice.result) > -1) {
            rerolls ++;
        }
    });

    if (rerolls > 0) {
        rollDice(object, rerolls, options, results, false);
    }
}

async function executeRoll(object, text, options, pool) {
    const description = `Rolling ${text} - ${pool} dice`;
    let results = new RollResults();
    results.initialPool = pool;

    await rollDice(object, pool, options, results);

    let resultText = `${results.successes} successes`;
    if (results.successes === 0 && results.botches > 0) {
        resultText = `<p style="color:red; font-weight: bold;">${resultText} (${results.botches} point botch)</p>`;
    } else {
        resultText = `<p style="color: green;">${resultText}</p>`;
    }


    CONFIG.ChatMessage.entityClass.create({
        user: game.user._id,
        speaker: ChatMessage.getSpeaker({ actor: object }),
        type: CONST.CHAT_MESSAGE_TYPES.IC,
        flavor: `${description}${resultText}`,
        content: results.dice.join(", "),
        sound: CONFIG.sounds.dice,
    });
}

async function doFixedPoolRoll($el, object) {
    let characteristic1 = $el.data('roll-characteristic1');
    let characteristic2 = $el.data('roll-characteristic2');
    let hasSpecialty = false;

    const text = $el.data('roll-text');

    let options = await bindings.popupForm('Roll pool', fullPath('templates/dialogs/shape-pool-roll.html'), {
        characteristics: [
            'strength',
            'dexterity',
            'stamina',
            'charisma',
            'manipulation',
            'appearance',
            'perception',
            'intelligence',
            'wits',
            ...Object.keys(skills),
        ],
        characteristic1,
        characteristic2,
    });

    characteristic1 = options.characteristic1;
    characteristic2 = options.characteristic2;
    hasSpecialty = (options.has_specialty || 0) === '1';

    let max = 0;
    if (characteristic1) {
        max += object.getCharacteristic(characteristic1);
    }
    if (characteristic2) {
        max += object.getCharacteristic(characteristic2);
    }
    max = max * 2;
    if (hasSpecialty) {
        max ++;
    }

    const pool = Math.min(parseInt($el.data('roll-pool')), max);
    await executeRoll(object, text, defaultRollOptions, pool);
}

// todo - ability to change attribute on roll
async function doRoll($el, object, advanced = false) {
    let pool = 0;
    let options = {...defaultRollOptions };
    let advancedOptions = advancedRollOptions;
    let attribute = $el.data('roll-attribute');
    const skill = $el.data('roll-skill');

    options = { ...defaultRollOptions };
    advancedOptions = advancedRollOptions;

    if (advanced) {
        // stunt dice (update willpower)
        // bonus charm dice
        // bonus non-charm dice
        // specialty? Yes/No
        // select attribute
        // essence - personal / peripheral (update anima)
        let advanced = await bindings.popupForm('Roll', fullPath('templates/dialogs/advanced-roll.html'), {
            hasSecondaryAttribute: object.data.type === 'lunar',
            attribute: attribute,
            skill: skill
        });

        advancedOptions = $.extend({...advancedRollOptions}, advanced);

        options.doubles = map(advancedOptions.doubles, ((num) => {
            return parseInt(num);
        }));

        options.rerolls = map(advancedOptions.rerolls, ((num) => {
            return parseInt(num);
        }));

        attribute = advancedOptions.attribute;
        advancedOptions.charm_dice = parseInt(advancedOptions.charm_dice);
        advancedOptions.non_charm_dice = parseInt(advancedOptions.non_charm_dice);
        advancedOptions.stunt_dice = parseInt(advancedOptions.stunt_dice);
        advancedOptions.specialty = parseInt(advancedOptions.specialty) === 1;
        advancedOptions.essence_personal = parseInt(advancedOptions.essence_personal);
        advancedOptions.essence_peripheral = parseInt(advancedOptions.essence_peripheral);
    }


    if (object.data.data[attribute]) {
        pool += parseInt(object.data.data[attribute].value);
    }
    if (object.data.data.skills[skill]) {
        pool += parseInt(object.data.data.skills[skill].level);
    }

    const charmDiceCap = object.getCharmDiceCap(attribute, skill, advancedOptions);
    if (advancedOptions.charm_dice > charmDiceCap) {
        advancedOptions.charm_dice = charmDiceCap;
    }

    pool += advancedOptions.charm_dice + advancedOptions.non_charm_dice + advancedOptions.stunt_dice;
    if (advancedOptions.specialty) {
        pool ++;
    }

    let text = attribute;
    if (skill) {
        text += ' + ' + skill;
    }

    await executeRoll(object, text, options, pool);
}

export function bindCharacterDiceRoller($doc, object) {
    $doc.find('.rollable').click(async function(evt) {
        if ($(this).data('roll-pool')) {
            await doFixedPoolRoll($(this), object);
        } else {
            await doRoll($(this), object, true);
        }
    });
}