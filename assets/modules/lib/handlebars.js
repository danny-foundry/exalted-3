import {fullPath} from "./paths.js";
import {renderDots} from "../custom/white-wolf.js";

export function handlebarsHelpers() {
    Handlebars.registerHelper("slugify", function(value) {
        return value.slugify({ strict: true });
    });

    Handlebars.registerHelper("numberFormat", (value) => {
        if (value >= 0) {
            return "+" + value;
        }
        return value;
    });

    Handlebars.registerHelper("json", (value) => {
        return JSON.stringify(value);
    });

    Handlebars.registerHelper('select', function(value, options) {
        var $el = $('<select />').html(options.fn(this));
        $el.find('[value="' + value + '"]').attr({selected: 'selected'});

        return $el.html();
    });

    Handlebars.registerHelper('object_value', function(object, key) {
        console.log(object, key);
        if (!object) {
            return '';
        }

        return object[key] || '';
    });

    Handlebars.registerHelper('is_checked', function(value, key = null) {
        if (!value) {
            return '';
        }

        let val = (key && key.name !== 'is_checked') ? (value[key] || false) : value;

        if (val) { return 'checked' }
        return '';
    });

    Handlebars.registerHelper('path', function(value) {
        return fullPath(value);
    });

    Handlebars.registerHelper('partial', function(value, opt) {
        return Handlebars.partials[fullPath(value)](opt.data.root);
    });

    Handlebars.registerHelper('render_additional_skill', function(key, skill, solar = false) {
        var $checkbox = $(`<input type="checkbox" name="data.additional_skills.${key}.favoured" value="1" />`);
        var $name = $(`<span class="skill-name"><input type="text" name="data.additional_skills.${key}.name" value="${skill.name}" /></span>`);
        var $dots = $(renderDots(skill.level, `data.additional_skills.${key}.level`, 0, 5));
        var $el = $('<div></div>');

        if (skill.favoured) {
            $checkbox.attr('checked', true);
        }

        if (solar) {
            $el.append($checkbox);
        }

        $el.append($name);
        $el.append($dots);

        return $el.html();
    });

    Handlebars.registerHelper('render_skill', function(key, skill, solar = false) {
        var $checkbox = $(`<input type="checkbox" name="data.skills.${key}.favoured" value="1" />`);
        var $name = $(`<span class="skill-name"><input type="hidden" name="data.skills.${key}.name" value="${skill.name}" />${skill.name}</span>`);
        var $dots = $(renderDots(skill.level, `data.skills.${key}.level`, 0, 5));
        var $el = $('<div></div>');
        var $roll = $('<button></button>');

        $roll.attr('data-roll-text', `Rolling ${skill.name}`);
        $roll.attr('data-roll-attribute', skill.attribute);
        $roll.attr('data-roll-skill', key);
        $roll.addClass('item-button');
        $roll.addClass('rollable');
        $roll.html('<i class="fas fa-dice"></i>');

        if (skill.favoured) {
            $checkbox.attr('checked', true);
        }

        if (solar) {
            $el.append($checkbox);
        }

        $el.append($roll);
        $el.append($name);
        $el.append($dots);

        return $el.html();
    });

    Handlebars.registerHelper('health_levels', function(health_levels) {
        let container = $('<div class="flexrow"></div>');

        if (!health_levels) {
            health_levels = {};
        }

        container.append(`<label class="d-block">0: <input type="number" name="data.health_levels.0" value="${health_levels["0"]}"</label>`);
        container.append(`<label class="d-block">-1: <input type="number" name="data.health_levels.-1" value="${health_levels["-1"]}"</label>`);
        container.append(`<label class="d-block">-2: <input type="number" name="data.health_levels.-2" value="${health_levels["-2"]}"</label>`);
        container.append(`<label class="d-block">-4: <input type="number" name="data.health_levels.-4" value="${health_levels["-4"]}"</label>`);

        return $('<div></div>').append(container).html();
    });

    Handlebars.registerHelper('health', function(health_levels, damage) {
        if (!damage) {
            damage = {};
        }

        if (!health_levels) {
            health_levels = {};
        }

        let boxes = [];
        let damagedLevels = [];
        damagedLevels = [
            ...new Array(Math.max(damage.aggravated || 0, 0)).fill('*'),
            ...new Array(Math.max(damage.lethal || 0, 0)).fill('x'),
            ...new Array(Math.max(damage.bashing || 0, 0)).fill('/'),
        ];

        let createHealthLevel = function(injuryPenalty, damage) {
            if (!damage) {
                damage = '';
            }

            let $box = $('<div class="dots"></div>');
            $box.append('<div class="dot box">' + damage + '</div>');
            $box.append('<div>' + injuryPenalty + '</div>');

            return $('<div></div>').append($box).html();
        };

        for (let i = 0; i < health_levels["0"]; i ++) {
            boxes.push(createHealthLevel(0, damagedLevels.shift()));
        }

        for (let i = 0; i < health_levels["-1"]; i ++) {
            boxes.push(createHealthLevel(-1, damagedLevels.shift()));
        }

        for (let i = 0; i < health_levels["-2"]; i ++) {
            boxes.push(createHealthLevel(-2, damagedLevels.shift()));
        }

        for (let i = 0; i < health_levels["-4"]; i ++) {
            boxes.push(createHealthLevel(-4, damagedLevels.shift()));
        }
        boxes.push(createHealthLevel('I', damagedLevels.shift()));

        let markup = '<div>' + boxes.join('') + '</div>';

        return $('<div></div>').append(markup).html();
    });

    Handlebars.registerHelper('boxes', function(value, name, min, max, actor, itemId) {
        return renderDots(value, name, min, max, actor, itemId, 'box');
    });

    Handlebars.registerHelper('dots', function(value, name, min, max, actor, itemId) {
        return renderDots(value, name, min, max, actor, itemId);
    });

    Handlebars.registerHelper('keyword_list', function(keywords) {
        var words = [];
        for (var keyword in keywords) {
            if (!keywords.hasOwnProperty(keyword)) continue;
            if (!keywords[keyword]) continue;

            words.push(keyword);
        }

        return words.join(", ");
    });
}

export function loadHandlebarsPartials() {
    // specify partials that need loading here - otherwise they appear to be inaccessible
    loadTemplates([
        // 'systems/example-system/templates/...',
        fullPath('templates/actors/partials/header.html'),
        fullPath('templates/actors/partials/front-page-left-column.html'),
        fullPath('templates/actors/partials/front-page-right-column.html'),
        fullPath('templates/actors/partials/attributes.html'),
        fullPath('templates/actors/partials/skills.html'),
        fullPath('templates/actors/partials/specialties.html'),
        fullPath('templates/actors/partials/merits.html'),
        fullPath('templates/actors/partials/willpower.html'),
        fullPath('templates/actors/partials/limit-break.html'),
        fullPath('templates/actors/partials/essence.html'),
        fullPath('templates/actors/partials/limit-trigger.html'),
        fullPath('templates/actors/partials/experience.html'),
        fullPath('templates/actors/partials/solar-experience.html'),
        fullPath('templates/actors/partials/weapons.html'),
        fullPath('templates/actors/partials/anima.html'),
        fullPath('templates/actors/partials/description.html'),
        fullPath('templates/actors/partials/intimacies.html'),
        fullPath('templates/actors/partials/health.html'),
        fullPath('templates/actors/partials/defenses.html'),
        fullPath('templates/actors/partials/equipment.html'),
        fullPath('templates/actors/partials/armour.html'),
        fullPath('templates/actors/partials/pools.html'),


        fullPath('templates/actors/partials/lunar/header.html'),
        fullPath('templates/actors/partials/lunar/attributes.html'),
        fullPath('templates/actors/partials/lunar/charms.html'),
        fullPath('templates/actors/partials/lunar/shapes.html'),
        fullPath('templates/actors/partials/lunar/shape-pools.html'),

        fullPath('templates/actors/partials/solar/header.html'),
        fullPath('templates/actors/partials/solar/skills.html'),
        fullPath('templates/actors/partials/solar/front-page-left-column.html'),
        fullPath('templates/actors/partials/solar/charms.html'),

        fullPath('templates/items/partials/ranged-accuracy.html'),
        fullPath('templates/items/partials/tags.html'),

        fullPath('templates/dialogs/confirm.html'),
        fullPath('templates/dialogs/health-and-damage.html'),
        fullPath('templates/dialogs/spend-essence.html'),
        fullPath('templates/dialogs/shape-pool-roll.html'),

        fullPath('templates/partials/debug-entity.html'),
    ]);
}